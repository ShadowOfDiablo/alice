# Alice's Adventures in Wonderland  
![Alice's Adventures in Wonderland](https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png)
## ALICE'S ADVENTURES IN WONDERLAND  

### BY LEWIS CARROLL
*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998  

[A BookVirtual Digital Edition, v1.2](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf&ved=2ahUKEwjh8qGzo5uAAxX_dKQEHQH0CwYQFnoECBUQAQ&usg=AOvVaw1g3f6kLbJNHl5RuqBttq4a)  
November, 2000
![Alice's Adventures in Wonderland](https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg)
<pre>
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.



Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?



Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not <em>more</em> than once a minute.



Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.



And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It <em>is</em> next time!"
  The happy voices cry.



Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.



Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
</pre>

# Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
--- 
1

![DOWN THE RABBIT-HOLE](https://www.gutenberg.org/files/19778/19778-h/images/p001.png)
## Chapter I
### DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, “ and what is  

---
35

![A CAUCUS-RACE AND A LONG TALE](https://www.gutenberg.org/files/19778/19778-h/images/p033.png)

&nbsp;&nbsp;&nbsp;&nbsp;Then they all crowded round her once more,  
while the Dodo solemnly presented the thimble,  
saying, "We beg your acceptance of this elegant  
thimble;" and, when it had finished this short  
speech, they all cheered.  

---
39

Alice replied eagerly, for she was always  
ready to talk about her pet. “ Dinah ’s our  
cat. And she’s such a capital one for catching  
mice you can ’t think ! And oh, I wish you  
could see her after the birds! Why, she ’ll eat  
a little bird as soon as look at it !”  
This speech caused a remarkable sensation  
among the party. Some of the birds hurried  
off at once : one old magpie began wrapping  
itself up very carefully, remarking, “ I really  
must be getting home ; the night-air doesn ’t  

---
83

&nbsp;&nbsp;&nbsp;&nbsp;“They all can,” said the Duchess ; “and  
most of ’em do.”  
&nbsp;&nbsp;&nbsp;&nbsp;“I don’t know of any that do,” Alice said  
very politely, feeling quite pleased to have got  
into a conversation.  
&nbsp;&nbsp;&nbsp;&nbsp;“You don’t know much,” said the Duchess ;  
“and that ’s a fact.”  

---
107

&nbsp;&nbsp;&nbsp;&nbsp;Alice did not quite know what to say to  
this: so she helped herself to some tea an  
bread-and-butter, and then turned to the Dor-  
mouse, and repeated her question. “Why did  
they live at the bottom of a well ?”  
&nbsp;&nbsp;&nbsp;&nbsp;The Dormouse again took a minute or two  
to think about it, and then said, “ It was a  
treacle-well.”  

***
154  

*"It does the boots and shoes,"* the Gryphon  
replied very solemnly.  
Alice was througly puzzled. "Does the  
boots and shoes!" she repeated in a wonder-  
ing tone.  
"Why, what are *your* shoes done with?"  
said the Gryphon. "I mean, what makes them  
so shiny?"  
Alice looked down at them, and considered  
a little before she gave her answer. "They're  
done with blacking, I believe."  
"Boots and shoes under the sea," the Gry-  
phon went on in a deep voice, "are done with  
whiting. Now you know."  
"And what are they made of?" Alice asked  
in a tone of great curiosity.  
"Soles and eels, of course," the Gryphon  
replied rather impatiently: "any shrimp could  
have told you that."  
"If I'd been the whiting," said Alice, whose  
thoughts were still running on the song, "I'd  
have said to the porpoise, 'Keep back, please:  

--- 
156

&nbsp;&nbsp;&nbsp;&nbsp;So Alice began telling them her adventures  
from the time when she first saw the White  
Rabbit: she was a little nervous about it just at  
first, the two creatures got so close to her, one  
on each side, and opened their eyes and mouths  
so very wide, but she gained courage as she  
went on. Her listeners were perfectly quiet  
till she got to the part about her repeating  
“*You are old, Father William,*” to the Cater-  
pillar, and the words all coming different, and  
then the Mock Turtle drew a long breath, and  
said, “That’s very curious.”  

&nbsp;&nbsp;&nbsp;&nbsp;“It’s all about as curious as it can be,” said  
the Gryphon.  

&nbsp;&nbsp;&nbsp;&nbsp;“It all came different!” the Mock Turtle  
repeated thoughtfully. “I should like to hear  
her try and repeat something now. Tell her  
to begin.” He looked at the Gryphon as if he  
thought it had some kind of authority over  
Alice.  

"Stand up and repeat '*Tis the voice of the  
sluggard,*’” said the Gryphon.  

---
188

![](https://www.gutenberg.org/files/19778/19778-h/images/p174.png)  
At this the whole pack rose up into the air,  
and came flying down upon her; she gave a  
